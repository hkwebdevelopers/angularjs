# Work-around for Angular JQlite lack of selectors
window.query = (selector) ->
  document.querySelectorAll selector

app = angular.module 'Slides', []

# ROUTING
app.config [
  '$routeProvider'
  '$locationProvider'
  (
    $routeProvider
  ) ->
    $routeProvider
      .when('/', {
        controller: 'SlideCtrl'
        templateUrl: 'html/main.html'
      }).otherwise({redirectTo: '/'})
]

# DIRECTIVES
nnKeyboardEvents =
  GO_NEXT: 'GO_NEXT'
  GO_PREV: 'GO_PREV'

nnKeyboardKeys =
  ARROW_LEFT: 37
  ARROW_RIGHT: 39
  SPACE_BAR: 32

app.directive 'nnKeyboardController', [
  ->
    {
    template: '<input type="input" style="position: absolute; left:-9999px" autofocus="true">'
    restrict: 'E'
    scope: {}
    link: (scope, element, attrs) ->
      angular.element(query attrs.container).bind('click', (e) ->
        element[0].childNodes[0].focus()
      )
    controller: [
      "$rootScope"
      "$element"
      (
        $rootScope
        $element
      ) ->
        $element.bind('keyup', (e) ->
          switch e.keyCode
            when nnKeyboardKeys.SPACE_BAR
              $rootScope.$broadcast nnKeyboardEvents.GO_NEXT
            when nnKeyboardKeys.ARROW_RIGHT
              $rootScope.$broadcast nnKeyboardEvents.GO_NEXT
            when nnKeyboardKeys.ARROW_LEFT
              $rootScope.$broadcast nnKeyboardEvents.GO_PREV
        )
      ]
    }
]

# CONTROLLERS
app.controller 'SlideCtrl', [
  '$scope'
  '$rootScope'
  '$timeout'
  (
    $scope
    $rootScope
    $timeout
  ) ->

    class SlideController
      # Properties
      scope:
        index: 0
      # Methods
      constructor: (@max) ->
      step: (val) =>
        @scope.index += val
        if @scope.index < 0
          @scope.index = 0
        else if @scope.index >= @max
          @scope.index = @max - 1
        if not $scope.phase
          $scope.$digest()
      # Convinience
      next: =>
        @step 1
      prev: =>
        @step -1

    # Setup
    slideController = new SlideController(9)

    $scope.slides = slideController.scope

    # Events
    $rootScope.$on nnKeyboardEvents.GO_NEXT, slideController.next
    $rootScope.$on nnKeyboardEvents.GO_PREV, slideController.prev

    # Init
    $timeout ->
      document.documentElement.className=document.documentElement.className.split(" no_fouc").join("")
    , 0

]