# Avoid `console` errors in browsers that lack a console.
(->
  noop = ->
  methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn']

  console = (window.console = window.console || {})

  while methods.length
    method = methods.pop()
    if not console[method]
      console[method] = noop
)()



