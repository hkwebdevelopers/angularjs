

app = angular.module("myApp", []);

//app.controller("buttonController", function ($scope, timerService) {
//
//    var START_LABEL =  "Start"
//    var RESET_LABEL =  "Reset"
//
//    $scope.isRunning = false;
//    $scope.startLabel = START_LABEL
//
//    $scope.start = function () {
//
//        $scope.isRunning = !$scope.isRunning;
//
//        $scope.startLabel = $scope.isRunning ? RESET_LABEL : START_LABEL;
//
//        if ($scope.isRunning) {
//            timerService.start();
//        } else {
//            timerService.reset();
//        }
//
//    }
//
//    $scope.split = function () {
//        timerService.split();
//    }
//
//});
//
//app.controller("splitController", function ($scope, $rootScope) {
//
//    var list = [];
//    var sessionCount = 1;
//
//    function addSplit(value) {
//        list.push({session: sessionCount, time: value});
//    }
//
//    // Scope
//    $scope.list = list;
//
//    // Events
//    $rootScope.$on("TIMER_RESET", function (b, e) {
//        addSplit(e.value)
//        sessionCount++
//    });
//
//    $rootScope.$on("TIMER_SPLIT", function (b, e) {
//        addSplit(e.value)
//    });
//
//});
//
//
//
//app.factory("timerService", function ($rootScope) {
//
//    var isRunning = false
//    var interval;
//
//    var startTime = null;
//
//    function start () {
//        if (isRunning) return
//
//        interval = setInterval(exec, 100);
//
//        startTime = new Date();
//        exec();
//
//        isRunning = true;
//    }
//
//    function reset () {
//        if (!isRunning) return
//
//        clearInterval(interval);
//        $rootScope.$broadcast("TIMER_RESET", {value: getDiff()});
//
//        isRunning = false;
//    }
//
//    function split () {
//        $rootScope.$broadcast("TIMER_SPLIT", {value: getDiff()});
//    }
//
//    function getDiff() {
//        var time = new Date();
//        var diff = time - startTime;
//        return diff;
//    }
//
//    function exec () {
//        $rootScope.$broadcast("TIMER_UPDATED", {value: getDiff()});
//    }
//
//    // Return Object
//    return {
//        start: start,
//        reset: reset,
//        split: split
//    }
//
//});
//
//
//
//app.directive("clock", function () {
//    return {
//        templateUrl: "clock.html",
//            restrict: "E",
//        scope: {},
//        link: function (scope, element, attrs) {
//        },
//        controller: function ($scope, $rootScope) {
//
//            function setTime(val) {
//                $scope.time = val;
//                if (!$rootScope.$$phase) {
//                    $scope.$digest();
//                }
//            }
//
//            setTime(0);
//
//            // Events
//            $rootScope.$on("TIMER_RESET", function (b, e) {
//                setTime(0);
//            });
//
//            $rootScope.$on("TIMER_UPDATED", function (b, e) {
//                setTime(e.value);
//            });
//
//        }
//    }
//});
//
//
//
//app.filter("timeFormat", function () {
//    return function (val) {
//        return val + ' ms'
//    }
//});